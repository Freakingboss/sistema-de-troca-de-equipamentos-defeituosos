drop database if exists Pecas;

create database Pecas;

use Pecas;

create table Peca(
    id int primary key auto_increment not null,
    nome varchar (50) not null,
    numeroDeSerie varchar (50) not null,
    modelo varchar (10) not null,
    trocado bit not null default 0,
    dataTroca timestamp,
    dataEstoque timestamp not null default now()
);

insert into Peca (nome, numeroDeSerie, modelo, dataEstoque) values ("Porca","SF1234567890sf","40mm",now());
insert into Peca (nome, numeroDeSerie, modelo, dataEstoque) values ("Porca","SF","40mm",now());
insert into Peca (nome, numeroDeSerie, modelo, dataEstoque) values ("Porca","SF111sf","40mm",now());
insert into Peca (nome, numeroDeSerie, modelo, dataEstoque) values ("Porca","SFsf","40mm",now());

insert into Peca (nome, numeroDeSerie, modelo, dataEstoque) values ("Parafuso","SF123","80mm",now());
insert into Peca (nome, numeroDeSerie, modelo, dataEstoque) values ("Guilhotina","sf890","40mm",now());




