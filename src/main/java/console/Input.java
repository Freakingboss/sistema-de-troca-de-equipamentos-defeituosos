package console;

import dominio.entrada.Peca;

import java.util.Scanner;

public class Input {

    public Peca obterEntrada(){

        final Scanner scanner = new Scanner(System.in);

        System.out.println("Insira o NOME da peca desejada: ");
        final String nome = scanner.nextLine();
        System.out.println("Insira o NUMERO DE SERIE da peca desejada: ");
        final String numeroDeSerie = scanner.nextLine();
        System.out.println("Insira o MODELO da peca desejada: ");
        final String modelo = scanner.nextLine();

        return novaPeca(nome, numeroDeSerie, modelo);
    }

    private Peca novaPeca(final String nome, final String numeroDeSerie, final String modelo){
        return new Peca() {
            @Override
            public String nome() {
                return nome;
            }

            @Override
            public String numeroDeSerie() {
                return numeroDeSerie;
            }

            @Override
            public String modelo() {
                return modelo;
            }
        };
    }

}
