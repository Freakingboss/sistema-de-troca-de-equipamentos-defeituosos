package console;

import dominio.saida.Resposta;

public class Output {

    public void saida(final Resposta resposta){
        System.out.println(resposta.codigo());
        System.out.println(resposta.protocolo());
        System.out.println(resposta.mensagem());
        System.out.println(resposta.peca().numeroDeSerie());
        System.out.println(resposta.peca().nome());
        System.out.println(resposta.peca().modelo());
    }

}
