package console;

import banco.dados.OperacaoEstoqueImplementacao;
import banco.dados.gerenciador.GerenciadorBD;
import banco.dados.gerenciador.GerenciadorBDImplementacao;
import dominio.entrada.Peca;
import dominio.estoque.OperarEmEstoque;
import dominio.trocar.peca.TrocaDePecasDefeituosas;
import dominio.trocar.peca.TrocaPecasDefeituosasImplementacao;

public class ConsoleMain {

    public void executar() {

        final Input input = new Input();

        final Peca peca = input.obterEntrada();

        final GerenciadorBD gerenciadorBD = new GerenciadorBDImplementacao();

        final OperarEmEstoque operarEmEstoque = new OperacaoEstoqueImplementacao(gerenciadorBD.novaConecao());

        final TrocaDePecasDefeituosas trocaDePecasDefeituosas = new TrocaPecasDefeituosasImplementacao
                .Builder()
                .comOperacaoEmEstoque(operarEmEstoque)
                .construir();

        final Output output = new Output();

        output.saida(trocaDePecasDefeituosas.trocarPeca(peca));

    }

}
