package web;

import banco.dados.OperacaoEstoqueImplementacao;
import banco.dados.gerenciador.GerenciadorBD;
import banco.dados.gerenciador.GerenciadorBDImplementacao;
import com.google.gson.Gson;
import dominio.entrada.Peca;
import dominio.estoque.OperarEmEstoque;
import dominio.saida.Resposta;
import dominio.trocar.peca.TrocaDePecasDefeituosas;
import dominio.trocar.peca.TrocaPecasDefeituosasImplementacao;
import spark.Spark;

import javax.xml.transform.Result;

public class WebMain {

    public void executar(){

        final Gson gson = new Gson();

        Spark.get("/", (request, response) -> "Sistema de Troca de pecas");

        final GerenciadorBD gerenciadorBD = new GerenciadorBDImplementacao();

        final OperarEmEstoque operarEmEstoque = new OperacaoEstoqueImplementacao(gerenciadorBD.novaConecao());

        final TrocaDePecasDefeituosas trocaDePecasDefeituosas = new TrocaPecasDefeituosasImplementacao
                .Builder()
                .comOperacaoEmEstoque(operarEmEstoque)
                .construir();

        Spark.post("/trocar-peca",(request, response) -> {
            final PecaImplementacao pecaImplementacao = gson.fromJson(request.body(), PecaImplementacao.class);
            final String json = gson.toJson(trocaDePecasDefeituosas.trocarPeca(pecaImplementacao));

            return json;
        });
    }

    static class PecaImplementacao implements Peca {

        private final String nome;
        private final String numeroDeSerie;
        private final String modelo;

        PecaImplementacao(String nome, String numeroDeSerie, String modelo) {
            this.nome = nome;
            this.numeroDeSerie = numeroDeSerie;
            this.modelo = modelo;
        }

        @Override
        public String nome() {
            return nome;
        }

        @Override
        public String numeroDeSerie() {
            return numeroDeSerie;
        }

        @Override
        public String modelo() {
            return modelo;
        }
    }

}
