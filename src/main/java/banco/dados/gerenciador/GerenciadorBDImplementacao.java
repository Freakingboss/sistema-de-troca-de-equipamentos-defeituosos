package banco.dados.gerenciador;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class GerenciadorBDImplementacao implements GerenciadorBD{

    private Connection connection;

    @Override
    public Connection novaConecao() {

        try {
            connection = DriverManager.getConnection("jdbc:mariadb://localhost/pecas", "root", "");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return connection;
    }

    @Override
    public boolean conectado() {
        try {
            return !connection.isClosed();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }
}
