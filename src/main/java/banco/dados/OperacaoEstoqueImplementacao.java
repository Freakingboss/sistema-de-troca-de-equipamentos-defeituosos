package banco.dados;

import dominio.entrada.Peca;
import dominio.estoque.OperarEmEstoque;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OperacaoEstoqueImplementacao implements OperarEmEstoque {

    private final Connection connection;

    public OperacaoEstoqueImplementacao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Peca> buscarPecas(Peca peca) {

        final String query = "SELECT nome, numeroDeSerie, modelo FROM Peca WHERE trocado = 0 AND nome = ? AND modelo = ?";
        final List<Peca> pecaList = new ArrayList<>();

        try {
            final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, peca.nome());
            statement.setString(2, peca.modelo());
            final ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()){

                final Peca novaPeca = novaPeca(
                        resultSet.getString("nome"),
                        resultSet.getString("numeroDeSerie"),
                        resultSet.getString("modelo"));

                pecaList.add(novaPeca);

            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return pecaList;
    }

    @Override
    public boolean atualizarEstoque(Peca peca) {

        final String query = "UPDATE Peca SET trocado = 1, dataTroca = NOW() WHERE numeroDeSerie = ?";
        boolean numeroDeColunasAlteradas = false;

        try {
            final PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setString(1, peca.numeroDeSerie());
            numeroDeColunasAlteradas = stmt.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return numeroDeColunasAlteradas;
    }

    private Peca novaPeca(final String nome, final String numeroDeSerie, final String modelo){
        return new Peca() {
            @Override
            public String nome() {
                return nome;
            }

            @Override
            public String numeroDeSerie() {
                return numeroDeSerie;
            }

            @Override
            public String modelo() {
                return modelo;
            }
        };
    }
}
