import console.ConsoleMain;
import web.WebMain;

public class Main {

    public static void main(String[] args){

        //final ConsoleMain consoleMain = new ConsoleMain();

        //consoleMain.executar();

        final WebMain webMain = new WebMain();

        webMain.executar();

    }

}
