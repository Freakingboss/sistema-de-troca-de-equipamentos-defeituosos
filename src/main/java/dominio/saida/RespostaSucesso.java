package dominio.saida;

import dominio.entrada.Peca;

final class RespostaSucesso implements Resposta{

    private final Integer codigo;
    private final String protocolo;
    private final String mensagem;

    @Override
    public String toString() {
        return "RespostaSucesso{" +
                "codigo=" + codigo +
                ", protocolo='" + protocolo + '\'' +
                ", mensagem='" + mensagem + '\'' +
                ", peca=" + peca +
                '}';
    }

    private final Peca peca;

    RespostaSucesso(Integer codigo, String protocolo, String mensagem, Peca peca) {
        this.codigo = codigo;
        this.protocolo = protocolo;
        this.mensagem = mensagem;
        this.peca = peca;
    }

    @Override
    public Integer codigo() {
        return codigo;
    }

    @Override
    public String protocolo() {
        return protocolo;
    }

    @Override
    public String mensagem() {
        return mensagem;
    }

    @Override
    public Peca peca() {
        return peca;
    }

}
