package dominio.saida;

import dominio.entrada.Peca;

public class FabricaRespostas {

    public static Resposta criarRespostaSucesso(final Peca peca){
        return new RespostaSucesso(
                CodigoStatus.PECA_TROCADA.obterCodigo(),
                Protocolo.PROTOCOLO_PECA_TROCADA.obterProtocolo(),
                "Peca trocada com sucesso!",
                peca
        );
    }

    public static Resposta criarRespostaRequisicaoInvalida(){
        return new RespostaImplementacao(
                CodigoStatus.REQUISICAO_INVALIDA.obterCodigo(),
                Protocolo.PROTOCOLO_REQUISICAO_INVALIDA.obterProtocolo(),
                "Peca invalida!"
        );

    }

    public static Resposta criarRespostaRequisicaoValida(){
        return new RespostaImplementacao(
                CodigoStatus.REQUISICAO_VALIDA.obterCodigo(),
                Protocolo.PROTOCOLO_REQUISICAO_VALIDA.obterProtocolo(),
                "Peca valida!"
        );

    }

    public static Resposta criarRespostaNaoContemPecaEstoque(){
        return new RespostaImplementacao(
                CodigoStatus.NAO_CONTEM_PECA_ESTOQUE.obterCodigo(),
                Protocolo.PROTOCOLO_NAO_CONTEM_PECA_ESTOQUE.obterProtocolo(),
                "Peca nao encontrada no estoque!"
        );
    }

}
