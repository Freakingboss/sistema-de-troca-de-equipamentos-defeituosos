package dominio.saida;

public enum Protocolo {

    PROTOCOLO_REQUISICAO_VALIDA("PROTOCOLO REQUISICAO VALIDA"),
    PROTOCOLO_PECA_TROCADA("PROTOCOLO PECA TROCADA"),

    PROTOCOLO_REQUISICAO_INVALIDA("PROTOCOLO REQUISICAO INVALIDA"),
    PROTOCOLO_NAO_CONTEM_PECA_ESTOQUE("PROTOCOLO NAO CONTEM PECA ESTOQUE");

    private final String protocolo;

    Protocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    public String obterProtocolo(){
        return protocolo;
    }
}
