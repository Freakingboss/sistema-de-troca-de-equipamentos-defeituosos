package dominio.saida;

import dominio.entrada.Peca;

final class RespostaImplementacao implements Resposta{

    private final Integer codigo;
    private final String protocolo;
    private final String mensagem;

    RespostaImplementacao(Integer codigo, String protocolo, String mensagem) {
        this.codigo = codigo;
        this.protocolo = protocolo;
        this.mensagem = mensagem;
    }

    @Override
    public Integer codigo() {
        return codigo;
    }

    @Override
    public String protocolo() {
        return protocolo;
    }

    @Override
    public String mensagem() {
        return mensagem;
    }

    @Override
    public Peca peca() {
        return new PecaNula();
    }

    static class PecaNula implements Peca{

        @Override
        public String nome() {
            return "";
        }

        @Override
        public String numeroDeSerie() {
            return "";
        }

        @Override
        public String modelo() {
            return "";
        }
    }
}
