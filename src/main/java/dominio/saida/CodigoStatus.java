package dominio.saida;

public enum CodigoStatus {

    REQUISICAO_VALIDA(1000),
    PECA_TROCADA(1001),

    REQUISICAO_INVALIDA(2000),
    NAO_CONTEM_PECA_ESTOQUE(2001);

    private final Integer codigo;

    CodigoStatus(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer obterCodigo(){
        return codigo;
    }
}
