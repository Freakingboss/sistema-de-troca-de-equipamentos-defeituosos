package dominio.saida;

import dominio.entrada.Peca;

public interface Resposta {

    Integer codigo();
    String protocolo();
    String mensagem();
    Peca peca();

}
