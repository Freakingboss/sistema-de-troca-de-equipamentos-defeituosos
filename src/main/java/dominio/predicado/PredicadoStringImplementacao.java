package dominio.predicado;

import java.util.regex.Pattern;

final class PredicadoStringImplementacao implements PredicadoString{

    @Override
    public boolean teste(final String valor, final String regex) {
        final Pattern regexCompilado = Pattern.compile(regex);
        return regexCompilado.matcher( valor ).find();
    }
}
