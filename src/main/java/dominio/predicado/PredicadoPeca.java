package dominio.predicado;

import dominio.entrada.Peca;

import java.util.List;

@FunctionalInterface
public interface PredicadoPeca {

    boolean contem(final Peca peca, final List<Peca> listaDePecas);
}
