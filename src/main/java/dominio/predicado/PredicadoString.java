package dominio.predicado;

@FunctionalInterface
public interface PredicadoString {

    boolean teste(final String valor, final String regex);

}
