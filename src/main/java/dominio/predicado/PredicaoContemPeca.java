package dominio.predicado;

import dominio.entrada.Peca;

import java.util.List;
import java.util.stream.Collectors;

final class PredicaoContemPeca implements PredicadoPeca{

    @Override
    public boolean contem(final Peca pecaEntrada, final List<Peca> listaDePecas) {

        return !listaDePecas
                .stream()
                .filter(peca -> peca.modelo().equals(pecaEntrada.modelo()))
                .filter(peca -> peca.nome().equals(pecaEntrada.nome()))
                .collect(Collectors.toList())
                .isEmpty();
    }

}
