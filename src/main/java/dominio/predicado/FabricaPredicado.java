package dominio.predicado;

public class FabricaPredicado {

    public static PredicadoString criarPredicadoString(){
        return new PredicadoStringImplementacao();
    }

    public static Predicado<Integer> criarPredicadoCodigoStatusValido(){return new PredicadoCodigoStatusValido();}
    
    public static PredicadoPeca criarPredicadoContemPeca(){return new PredicaoContemPeca();}

}
