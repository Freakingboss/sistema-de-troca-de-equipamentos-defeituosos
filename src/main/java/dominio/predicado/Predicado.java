package dominio.predicado;

public interface Predicado<T> {

    boolean avaliar(final T t);

}
