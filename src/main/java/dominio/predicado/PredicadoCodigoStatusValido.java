package dominio.predicado;

import dominio.saida.CodigoStatus;

final class PredicadoCodigoStatusValido implements Predicado<Integer>{
    @Override
    public boolean avaliar(final Integer valor) {
        if(valor.equals(CodigoStatus.REQUISICAO_VALIDA.obterCodigo()))
            return true;
        return false;
    }
}
