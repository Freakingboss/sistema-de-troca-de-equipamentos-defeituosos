package dominio.entrada;

public interface Peca {

    String nome();
    String numeroDeSerie();
    String modelo();

}
