package dominio.estoque;

import dominio.entrada.Peca;

import java.util.List;

public interface OperarEmEstoque {

    List<Peca> buscarPecas(final Peca peca);

    boolean atualizarEstoque(final Peca peca);

}
