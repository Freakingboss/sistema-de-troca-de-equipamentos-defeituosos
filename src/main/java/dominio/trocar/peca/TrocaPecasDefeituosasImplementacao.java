package dominio.trocar.peca;

import dominio.estoque.OperarEmEstoque;
import dominio.entrada.Peca;
import dominio.predicado.FabricaPredicado;
import dominio.saida.FabricaRespostas;
import dominio.saida.Resposta;

import java.util.List;

public class TrocaPecasDefeituosasImplementacao implements TrocaDePecasDefeituosas {

    private final OperarEmEstoque operarEmEstoque;

    TrocaPecasDefeituosasImplementacao(OperarEmEstoque operarEmEstoque) {
        this.operarEmEstoque = operarEmEstoque;
    }

    @Override
    public Resposta trocarPeca(Peca peca) {

        final ValidarEntrada validarEntrada = new ValidarEntrada();
        final Resposta resposta = validarEntrada.validarPeca(peca);

        if(!FabricaPredicado.criarPredicadoCodigoStatusValido().avaliar(resposta.codigo()))
            return FabricaRespostas.criarRespostaRequisicaoInvalida();

        final List<Peca> listaDePecas = operarEmEstoque.buscarPecas(peca);

        if(!FabricaPredicado.criarPredicadoContemPeca().contem(peca, listaDePecas))
            return FabricaRespostas.criarRespostaNaoContemPecaEstoque();

        final Peca pecaDeTroca = listaDePecas
                .stream()
                .filter(novaPeca -> novaPeca.modelo().equals(peca.modelo()))
                .findFirst()
                .get();

        operarEmEstoque.atualizarEstoque(pecaDeTroca);

        return FabricaRespostas.criarRespostaSucesso(pecaDeTroca);
    }

    public static class Builder{

        private OperarEmEstoque operarEmEstoque;

        public Builder comOperacaoEmEstoque(final OperarEmEstoque operarEmEstoque){
            this.operarEmEstoque = operarEmEstoque;
            return this;
        }

        public TrocaDePecasDefeituosas construir(){
            return new TrocaPecasDefeituosasImplementacao(operarEmEstoque);
        }
    }
}
