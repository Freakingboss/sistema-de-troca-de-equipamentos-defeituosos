package dominio.trocar.peca;

import dominio.entrada.Peca;
import dominio.saida.Resposta;

@FunctionalInterface
public interface TrocaDePecasDefeituosas {

    Resposta trocarPeca(final Peca peca);

}
