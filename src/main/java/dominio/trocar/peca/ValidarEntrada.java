package dominio.trocar.peca;

import dominio.entrada.Peca;
import dominio.predicado.FabricaPredicado;
import dominio.predicado.PredicadoString;
import dominio.saida.FabricaRespostas;
import dominio.saida.Resposta;

final class ValidarEntrada {

    private final static String REGEX_ALFANUMERICO = "^([A-Za-z0-9])+$";
    private final static String REGEX_NUMERICO = "^([0-9])+$";
    private final static String REGEX_STRING_VAZIA = "^$";

    Resposta validarPeca(final Peca peca){

        final PredicadoString predicadoString = FabricaPredicado.criarPredicadoString();

        if(predicadoString.teste(peca.nome(), REGEX_STRING_VAZIA))
            return FabricaRespostas.criarRespostaRequisicaoInvalida();

        if(predicadoString.teste(peca.modelo(), REGEX_STRING_VAZIA))
            return FabricaRespostas.criarRespostaRequisicaoInvalida();

        if(predicadoString.teste(peca.numeroDeSerie(), REGEX_ALFANUMERICO))
            return FabricaRespostas.criarRespostaRequisicaoValida();

        if(predicadoString.teste(peca.numeroDeSerie(), REGEX_NUMERICO))
            return FabricaRespostas.criarRespostaRequisicaoValida();

        return FabricaRespostas.criarRespostaRequisicaoInvalida();
    }

}
