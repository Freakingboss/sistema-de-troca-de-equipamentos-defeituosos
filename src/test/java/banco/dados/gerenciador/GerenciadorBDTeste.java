package banco.dados.gerenciador;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class GerenciadorBDTeste {

    @Test
    @DisplayName("Testando conexao de BD")
    public void testandoConexaoBD(){

        final GerenciadorBD gerenciadorBD = new GerenciadorBDImplementacao();

        try {
            final Connection connection = gerenciadorBD.novaConecao();

            gerenciadorBD.conectado();

            connection.close();

            Assertions.assertTrue(true);
        } catch (SQLException throwables) {

            throwables.printStackTrace();

            Assertions.assertTrue(false);
        }

    }

}
