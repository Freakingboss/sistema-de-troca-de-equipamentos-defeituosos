package dominio.predicado;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class PredicadoCodigoStatusValidoTeste {

    @Test
    @DisplayName("Valor do codigo de status valido")
    public void predicadoValido(){

        final Integer valor = 1000;

        Assertions.assertTrue(FabricaPredicado.criarPredicadoCodigoStatusValido().avaliar(valor));
    }

    @Test
    @DisplayName("Valor do codigo de status invalido")
    public void predicadoInvalido(){

        final Integer valor = 98765432;

        Assertions.assertFalse(FabricaPredicado.criarPredicadoCodigoStatusValido().avaliar(valor));
    }

}
