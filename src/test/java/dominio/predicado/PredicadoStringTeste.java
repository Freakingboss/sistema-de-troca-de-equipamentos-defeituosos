package dominio.predicado;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class PredicadoStringTeste {

    @Test
    @DisplayName("String deu Match com a expressao regular")
    public void predicadoValido(){

        final String alfaNumerico = "SF1234567890sf";
        final String expressaoRegular = "^([A-Za-z0-9])+$";
        final PredicadoString predicadoString = new PredicadoStringImplementacao();

        Assertions.assertTrue(predicadoString.teste(alfaNumerico, expressaoRegular));
    }

    @Test
    @DisplayName("String nao deu Match com a expressao regular")
    public void predicadoInvalido(){

        final String alfaNumerico = "SF12345*67890sf";
        final String expressaoRegular = "^([A-Za-z0-9])+$";
        final PredicadoString predicadoString = new PredicadoStringImplementacao();

        Assertions.assertFalse(predicadoString.teste(alfaNumerico, expressaoRegular));

    }

}
