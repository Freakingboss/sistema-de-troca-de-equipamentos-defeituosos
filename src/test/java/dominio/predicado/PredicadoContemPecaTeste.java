package dominio.predicado;

import dominio.NovaPecaTeste;
import dominio.entrada.Peca;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class PredicadoContemPecaTeste {

    @Test
    @DisplayName("Contem peca no estoque")
    public void predicadoValido(){

        final Peca peca = novaPeca("Peca1","SF1234567890sf","80mm");
        final Peca peca2 = novaPeca("Peca2","SF1290sf","90mm");

        List<Peca> listaDePecas = new ArrayList<>();
        listaDePecas.add(peca);
        listaDePecas.add(peca2);

        final PredicadoPeca predicadoPeca = FabricaPredicado.criarPredicadoContemPeca();

        Assertions.assertTrue(predicadoPeca.contem(peca, listaDePecas));

    }

    @Test
    @DisplayName("Nao contem peca no estoque")
    public void predicadoInvalido(){

        final Peca peca = novaPeca("Peca1","SF1234567890sf","80mm");
        final Peca peca2 = novaPeca("Peca2","SF1290sf","90mm");

        List<Peca> listaDePecas = new ArrayList<>();
        listaDePecas.add(peca2);

        final PredicadoPeca predicadoPeca = FabricaPredicado.criarPredicadoContemPeca();

        Assertions.assertFalse(predicadoPeca.contem(peca, listaDePecas));

    }

    @Test
    @DisplayName("Nao contem peca no estoque de mesmo nome")
    public void predicadoInvalidoPecasNomeDiferentes(){

        final Peca peca = novaPeca("Peca1","SF1234567890sf","80mm");
        final Peca peca2 = novaPeca("Peca2","SF1290sf","90mm");
        final Peca peca3 = novaPeca("Peca3","SF1290sf","80mm");

        List<Peca> listaDePecas = new ArrayList<>();
        listaDePecas.add(peca2);
        listaDePecas.add(peca3);

        final PredicadoPeca predicadoPeca = FabricaPredicado.criarPredicadoContemPeca();

        Assertions.assertFalse(predicadoPeca.contem(peca, listaDePecas));

    }

    private Peca novaPeca(final String nome, final String numeroDeSerie, final String modelo){

        return new NovaPecaTeste().novaPeca(nome, numeroDeSerie, modelo);

    }
}
