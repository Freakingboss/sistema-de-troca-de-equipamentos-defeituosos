package dominio.trocar.peca;

import dominio.NovaPecaTeste;
import dominio.entrada.Peca;
import dominio.saida.CodigoStatus;
import dominio.saida.Protocolo;
import dominio.saida.Resposta;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ValidarEntradaTeste {

    @Test
    @DisplayName("Cenario Requisicao Valida")
    public void requisicaoValida(){

        final Peca peca = novaPeca("porca","asdf1234","40mm");

        final ValidarEntrada validarEntrada = new ValidarEntrada();

        final Resposta resposta = validarEntrada.validarPeca(peca);

        Assertions.assertEquals(CodigoStatus.REQUISICAO_VALIDA.obterCodigo(), resposta.codigo());

        Assertions.assertEquals(Protocolo.PROTOCOLO_REQUISICAO_VALIDA.obterProtocolo(), resposta.protocolo());

        Assertions.assertEquals("Peca valida!", resposta.mensagem());

        Assertions.assertEquals("", resposta.peca().modelo());

        Assertions.assertEquals("", resposta.peca().numeroDeSerie());

        Assertions.assertEquals("", resposta.peca().nome());

    }

    @Test
    @DisplayName("Cenario Requisicao com Nome Vazio")
    public void requisicaoComNomeVazio(){

        final Peca peca = novaPeca("","asdf1234","40mm");

        final ValidarEntrada validarEntrada = new ValidarEntrada();

        final Resposta resposta = validarEntrada.validarPeca(peca);

        Assertions.assertEquals(CodigoStatus.REQUISICAO_INVALIDA.obterCodigo(), resposta.codigo());

        Assertions.assertEquals(Protocolo.PROTOCOLO_REQUISICAO_INVALIDA.obterProtocolo(), resposta.protocolo());

    }

    @Test
    @DisplayName("Cenario Requisicao com Modelo Vazio")
    public void requisicaoComModeloVazio(){

        final Peca peca = novaPeca("porca","asdf1234","");

        final ValidarEntrada validarEntrada = new ValidarEntrada();

        final Resposta resposta = validarEntrada.validarPeca(peca);

        Assertions.assertEquals(CodigoStatus.REQUISICAO_INVALIDA.obterCodigo(), resposta.codigo());

        Assertions.assertEquals(Protocolo.PROTOCOLO_REQUISICAO_INVALIDA.obterProtocolo(), resposta.protocolo());

    }

    @Test
    @DisplayName("Cenario Requisicao com Numero de Serie Invalido")
    public void requisicaoComNumeroSerieInvalido(){

        final Peca peca = novaPeca("porca","asdf______1234","40mm");

        final ValidarEntrada validarEntrada = new ValidarEntrada();

        final Resposta resposta = validarEntrada.validarPeca(peca);

        Assertions.assertEquals(CodigoStatus.REQUISICAO_INVALIDA.obterCodigo(), resposta.codigo());

        Assertions.assertEquals(Protocolo.PROTOCOLO_REQUISICAO_INVALIDA.obterProtocolo(), resposta.protocolo());

    }

    private Peca novaPeca(final String nome, final String numeroDeSerie, final String modelo){

        return new NovaPecaTeste().novaPeca(nome, numeroDeSerie, modelo);

    }

}
