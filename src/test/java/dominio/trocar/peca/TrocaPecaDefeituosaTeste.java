package dominio.trocar.peca;

import dominio.NovaPecaTeste;
import dominio.entrada.Peca;
import dominio.estoque.OperarEmEstoque;
import dominio.saida.CodigoStatus;
import dominio.saida.Protocolo;
import dominio.saida.Resposta;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TrocaPecaDefeituosaTeste {

    @Test
    @DisplayName("Cenario Troca de peca com sucesso")
    public void cenarioTrocaDePeca(){

        final Peca peca = novaPeca("Porca","asdf1234","40mm");

        final TrocaDePecasDefeituosas trocaDePecasDefeituosas = new TrocaPecasDefeituosasImplementacao
                .Builder()
                .comOperacaoEmEstoque(novaOperacaoEmEstoque())
                .construir();

        final Resposta resposta = trocaDePecasDefeituosas.trocarPeca(peca);

        Assertions.assertEquals(CodigoStatus.PECA_TROCADA.obterCodigo(), resposta.codigo());

        Assertions.assertEquals(Protocolo.PROTOCOLO_PECA_TROCADA.obterProtocolo(), resposta.protocolo());

    }

    @Test
    @DisplayName("Cenario Troca de peca requisicao invalida")
    public void cenarioTrocaDePecaRequisicaoInvalida(){

        final Peca peca = novaPeca("Porca","asdf12*34","40mm");

        final TrocaDePecasDefeituosas trocaDePecasDefeituosas = new TrocaPecasDefeituosasImplementacao
                .Builder()
                .comOperacaoEmEstoque(novaOperacaoEmEstoque())
                .construir();

        final Resposta resposta = trocaDePecasDefeituosas.trocarPeca(peca);

        Assertions.assertEquals(CodigoStatus.REQUISICAO_INVALIDA.obterCodigo(), resposta.codigo());

        Assertions.assertEquals(Protocolo.PROTOCOLO_REQUISICAO_INVALIDA.obterProtocolo(), resposta.protocolo());

    }

    @Test
    @DisplayName("Cenario peca nao encontrada")
    public void cenarioPecaNaoEncontrada(){

        final Peca peca = novaPeca("Porca","asdf1234","80mm");

        final TrocaDePecasDefeituosas trocaDePecasDefeituosas = new TrocaPecasDefeituosasImplementacao
                .Builder()
                .comOperacaoEmEstoque(novaOperacaoEmEstoque())
                .construir();

        final Resposta resposta = trocaDePecasDefeituosas.trocarPeca(peca);

        Assertions.assertEquals(CodigoStatus.NAO_CONTEM_PECA_ESTOQUE.obterCodigo(), resposta.codigo());

        Assertions.assertEquals(Protocolo.PROTOCOLO_NAO_CONTEM_PECA_ESTOQUE.obterProtocolo(), resposta.protocolo());

    }

    private Peca novaPeca(final String nome, final String numeroDeSerie, final String modelo){

        return new NovaPecaTeste().novaPeca(nome, numeroDeSerie, modelo);

    }

    private OperarEmEstoque novaOperacaoEmEstoque(){

        return new OperarEmEstoque() {

            private List<Peca> listaDePecas = new ArrayList<>();

            @Override
            public List<Peca> buscarPecas(final Peca pecaBusca) {

                final Peca peca = novaPeca("Porca","asdf1234","40mm");
                final Peca peca2 = novaPeca("Parafuso","SF1290sf","90mm");

                listaDePecas.add(peca);
                listaDePecas.add(peca2);

                return listaDePecas
                        .stream()
                        .filter(peca1 -> peca1.modelo().equals(pecaBusca.modelo()))
                        .collect(Collectors.toList());
            }

            @Override
            public boolean atualizarEstoque(Peca peca) {
                return false;

            }
        };
    }

}
