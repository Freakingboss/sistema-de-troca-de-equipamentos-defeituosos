package dominio;

import dominio.entrada.Peca;

public class NovaPecaTeste {

    public Peca novaPeca(final String nomePeca, final String numeroDeSerie, final String modelo){

        return new Peca() {
            @Override
            public String nome() {
                return nomePeca;
            }

            @Override
            public String numeroDeSerie() {
                return numeroDeSerie;
            }

            @Override
            public String modelo() {
                return modelo;
            }
        };

    }
}
